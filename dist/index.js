"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRoute = exports.addRoute = void 0;
const pageRoutes = [];
const addRoute = (routeKey, getPageComponentCallaback) => {
    if (getPageComponentCallaback) {
        const route = [routeKey, getPageComponentCallaback];
        pageRoutes.push(route);
    }
};
exports.addRoute = addRoute;
const getRoute = (path) => {
    const finder = ([key]) => key === path;
    return pageRoutes.find(finder);
};
exports.getRoute = getRoute;
//# sourceMappingURL=index.js.map