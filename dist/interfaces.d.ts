import { FunctionComponent } from 'preact';
import { PageData } from '@dixid/front-services/dist/interfaces';
export declare type ROUTE_TYPE_PAGE = 'page';
export declare type ROUTE_TYPE_JS = 'js';
export declare type ROUTE_TYPE_API = 'api';
export declare type ROUTE_TYPE_FILE = 'file';
export declare type ROUTE_TYPE_IMAGE = 'image';
export declare type RouteType = ROUTE_TYPE_FILE | ROUTE_TYPE_IMAGE | ROUTE_TYPE_API | ROUTE_TYPE_JS | ROUTE_TYPE_PAGE;
export declare type GetFormattedRoutePath = () => string;
export declare type GetRouteContent<T> = (content: T) => void;
export declare type GetRouteContentCallback<T, P> = (getRouteContent: GetRouteContent<T>, options?: P) => void;
export declare type IsRoute = (path: string) => boolean;
export declare type AddRoute<T, P> = (routeKey: string, getRouteContentCallback?: GetRouteContentCallback<T, P>) => void;
export declare type Route<T, P> = [
    routeKey: string,
    getRouteContentCallback: GetRouteContentCallback<T, P>,
    routeType?: RouteType
];
export declare type GetRoute<T, P> = (path: string, getFormattedRoutePath?: GetFormattedRoutePath) => Route<T, P> | undefined;
export interface PageProps {
    pageData: PageData;
}
export declare type PageComponent = FunctionComponent<PageProps>;
export interface PageOptions {
    method: 'GET' | 'OPTIONS';
    query: string;
}
export declare type GetPageComponent = GetRouteContent<PageComponent>;
export declare type GetPageComponentCallback = GetRouteContentCallback<PageComponent, PageOptions>;
export declare type AddPageRoute = AddRoute<PageComponent, PageOptions>;
export declare type PageRoute = Route<PageComponent, PageOptions>;
export declare type PageRoutes = PageRoute[];
export declare type GetPageRoute = GetRoute<PageComponent, PageOptions>;
//# sourceMappingURL=interfaces.d.ts.map