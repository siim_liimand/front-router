import { AddPageRoute, PageRoutes, PageRoute, GetPageRoute } from './interfaces';

const pageRoutes: PageRoutes = [];

export const addRoute: AddPageRoute = (routeKey, getPageComponentCallaback): void => {
  if (getPageComponentCallaback) {
    const route: PageRoute = [routeKey, getPageComponentCallaback];
    pageRoutes.push(route);
  }
};

export const getRoute: GetPageRoute = (path) => {
  const finder = ([key]: PageRoute) => key === path;
  return pageRoutes.find(finder);
};
