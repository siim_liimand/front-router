import { FunctionComponent } from 'preact';
import { PageData } from '@dixid/front-services/dist/interfaces';

export type ROUTE_TYPE_PAGE = 'page';
export type ROUTE_TYPE_JS = 'js';
export type ROUTE_TYPE_API = 'api';
export type ROUTE_TYPE_FILE = 'file';
export type ROUTE_TYPE_IMAGE = 'image';
export type RouteType = ROUTE_TYPE_FILE | ROUTE_TYPE_IMAGE | ROUTE_TYPE_API | ROUTE_TYPE_JS | ROUTE_TYPE_PAGE;

export type GetFormattedRoutePath = () => string;
export type GetRouteContent<T> = (content: T) => void;
export type GetRouteContentCallback<T, P> = (getRouteContent: GetRouteContent<T>, options?: P) => void;
export type IsRoute = (path: string) => boolean;
export type AddRoute<T, P> = (routeKey: string, getRouteContentCallback?: GetRouteContentCallback<T, P>) => void;
export type Route<T, P> = [
  routeKey: string,
  getRouteContentCallback: GetRouteContentCallback<T, P>,
  routeType?: RouteType,
];
export type GetRoute<T, P> = (path: string, getFormattedRoutePath?: GetFormattedRoutePath) => Route<T, P> | undefined;

export interface PageProps {
  pageData: PageData;
}
export type PageComponent = FunctionComponent<PageProps>;
export interface PageOptions {
  method: 'GET' | 'OPTIONS';
  query: string;
}
export type GetPageComponent = GetRouteContent<PageComponent>;
export type GetPageComponentCallback = GetRouteContentCallback<PageComponent, PageOptions>;
export type AddPageRoute = AddRoute<PageComponent, PageOptions>;
export type PageRoute = Route<PageComponent, PageOptions>;
export type PageRoutes = PageRoute[];
export type GetPageRoute = GetRoute<PageComponent, PageOptions>;
